package at.wofl.games.collission;

import java.util.ArrayList;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class CarActor {
	private float x, y;
	private int dir;
	private Shape shape;
	private Image car;
	private ArrayList<CirclesActor> collissionPartners;
	
	public CarActor(float x, float y, int dir) throws SlickException{
		super();
		this.x = x;
		this.y = y;
		this.dir = dir;
		this.shape = new Rectangle(this.x, this.y, 90, 90);
		this.car = new Image("testdata/cursor.png");
		this.collissionPartners = new ArrayList<>();
	}
	
	public void move(){
		if (dir ==1){
			this.x ++;
		}
		else{
			this.x --;
		}
	this.shape.setX(this.x);
	}
	
	
	public void render(Graphics g){
		
		g.fillRect(this.x, this.y, 70, 70);
		g.draw(this.shape);
	}
	
	public void addCollissionPartner(CirclesActor actor){
		this.collissionPartners.add(actor);
	}
}
