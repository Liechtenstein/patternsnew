package at.wofl.games.collission;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class CirclesActor {
	private float x, y;
	private int dir;
	private Shape shape;
	
	public CirclesActor(float x, float y, int dir){
		super();
		this.x = x;
		this.y = y;
		this.dir = dir;
		this.shape = new Circle(this.x, this.y, 110, 110);
	}
	
	public void move(){
		if (dir ==1){
			this.x ++;
		}
		else{
			this.x --;
		}
		this.shape.setX(this.x);
	}
	
	public void render(Graphics g){
		
		g.drawOval(this.x, this.y, 70, 70);
		g.draw(this.shape);
	
	}
}
