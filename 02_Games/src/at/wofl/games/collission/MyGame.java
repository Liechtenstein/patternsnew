package at.wofl.games.collission;

import java.util.ArrayList;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AlphaMapTest;

import at.wofl.games.secondgame.CircleActor;

public class MyGame extends BasicGame {
private CarActor car1;
private ArrayList<CirclesActor> circles;
private CirclesActor ca;

	public MyGame(String MyGame) {
		super("My first Game");
		
	}
	
//--------------------------------------------------------------------------------------------------------
	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		this.car1.render(g);
		for(CirclesActor circle: this.circles){
			circle.render(g);
		}
	
	}
	
//--------------------------------------------------------------------------------------------------------
	@Override
	public void init(GameContainer gc) throws SlickException {
		this.car1 = new CarActor(100,100,1);
		this.circles = new ArrayList<>();
		ca = new CirclesActor(10,10,2);
		this.circles.add(ca);
		this.car1.addCollissionPartner(ca);
		
		ca = new CirclesActor(104,10,2);
		this.circles.add(ca);
		this.car1.addCollissionPartner(ca);
		
		ca = new CirclesActor(10,140,2);
		this.circles.add(ca);
		this.car1.addCollissionPartner(ca);
		
		
		gc.setTargetFrameRate(60);
	}
	
//--------------------------------------------------------------------------------------------------------
	@Override
	public void update(GameContainer gc, int millisSinceLastCall) throws SlickException {
		this.car1.move();
		for (CirclesActor circle: this.circles){
			circle.move();
		}
		
	}

//--------------------------------------------------------------------------------------------------------
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MyGame("firstgame"));
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
