package at.wofl.games.firstgame;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AlphaMapTest;

public class MyGame extends BasicGame {
private int x,y;
private int direction; 
private int rx,ry;
private int rounddirection;
private int tx, ty;
private int twodirection;
private int ovadirection;
private int ox, oy;
private int points;


	public MyGame(String MyGame) {
		super("My first Game");
		// TODO Auto-generated constructor stub
	}
	
//--------------------------------------------------------------------------------------------------------
	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		g.setColor(Color.red);
		g.fillRect(this.x, this.y, 100, 100);
		
		g.setColor(Color.blue);
		g.fillRoundRect(this.rx, ry, 60, 60, 60);
		
		g.setColor(Color.yellow);
		g.fillRoundRect(this.tx, ty, 40, 40, 40);
		
		g.setColor(Color.cyan);
		g.fillOval(this.ox, this.oy, 50, 40);
		
		g.setColor(Color.white);
		g.drawString("Points: " + points, 200, 10);
	}
	
//--------------------------------------------------------------------------------------------------------
	@Override
	public void init(GameContainer gc) throws SlickException {
		this.x = 0;
		this.y = 0;
		this.direction = 1;

		this.rx = 0;
		this.ry = 300;
		this.rounddirection = 1;
		
		this.tx = 700;
		this.ty = 200;
		this.twodirection = 1;
		
		this.ox = 300;
		this.oy= 0;
		this.ovadirection = 1;
	}
	
//--------------------------------------------------------------------------------------------------------
	@Override
	public void update(GameContainer gc, int millisSinceLastCall) throws SlickException {
		if (direction==1){
				this.x++;
				if(this.x == 700)
				{
					direction=2;
				}
		}
		if (direction==2){
				this.y++;
				if(this.y == 500)
				{
					direction=3;
				}
		}
		if (direction==3){
				this.x--;
				if(this.x == 0)
				{
					direction=4;
				}
		}
		if (direction==4){
				this.y--;
				if(this.y == 0)
				{
					direction=1;
				}
		}
		
		if(rounddirection==1){
				this.rx++;
				if(this.rx ==700)
				{
					rounddirection=2;
				}
		}
		if(rounddirection==2){
				this.rx--;
				if(this.rx==0)
				{
					rounddirection=1;
				}
		}
		if(twodirection==1){
				this.tx--;
				if(this.tx==0)
				{
					twodirection=2;
				}
		}
		if(twodirection==2){
				this.tx++;
				if(this.tx==700)
				{
					twodirection=1;
				}
		}
		
		if(ovadirection ==1){
			this.oy++;
			if(this.oy==700)
			{
				this.oy=0;
			}
		}
	}
	
//--------------------------------------------------------------------------------------------------------
	@Override
		public void mouseClicked(int button, int x, int y, int clickCount) {
			// TODO Auto-generated method stub
			super.mouseClicked(button, x, y, clickCount);
			
			System.out.println("Button; " + button + "x:" + x);
			
			//Points f�r Quadrat:
			if(x>= this.x && x<this.x + 100)
			{	
				points+= 150;
			}
			
			//Points f�r blauer kreis
			if(x>= this.rx && x<this.rx +60)
			{
				points+= 200;
			}
			
			//Points f�r gelber Kreis
			if(x>= this.tx && x<this.tx +40)
			{
				points+= 500;
			}
			
			//Points f�r Oval_Circle
			if(x>= this.ox && x<this.ox +40)
			{
				points+= 1000;
			}
		}
	
//--------------------------------------------------------------------------------------------------------
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MyGame("firstgame"));
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
