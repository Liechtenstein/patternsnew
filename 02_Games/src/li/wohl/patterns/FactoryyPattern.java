package li.wohl.patterns;

public class FactoryyPattern {

	public AbstractPatternActor getShape(String ActorType, StrategyyPattern strategy) {
		if (ActorType == null) {
			return null;
		}
		if (ActorType.contentEquals("OVAL")) {
			return new Oval_Circle(strategy);
		} else if (ActorType.equalsIgnoreCase("Rectangle")) {
			return new Rectangle(strategy);
		}
		return null;
	}
}
