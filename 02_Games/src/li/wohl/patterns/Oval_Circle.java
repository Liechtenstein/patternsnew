package li.wohl.patterns;

import org.newdawn.slick.GameContainer;

public class Oval_Circle extends AbstractPatternActor{
	private int a,b;
	private StrategyyPattern strategy;
	
	public Oval_Circle(StrategyyPattern strategy) {
		super(strategy);
		
		this.strategy = strategy;
		this.a = 100;
		this.b = 100;
	}

	@Override
	public void inform() {
		System.out.println("I AM 1 OVAL");
		
	}

	@Override
	public void draw(org.newdawn.slick.Graphics g) {
		// TODO Auto-generated method stub
		g.drawOval(strategy.getX(), strategy.getY(), this.a, this.b);
	}

	@Override
	public void move(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		
	}
	
	
}
