package li.wohl.patterns;

import org.newdawn.slick.GameContainer;

public interface StrategyyPattern {

	public void moving(GameContainer gc, int delta);
	public float getX();
	public float getY();
}
