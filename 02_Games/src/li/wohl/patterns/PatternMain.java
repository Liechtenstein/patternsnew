package li.wohl.patterns;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class PatternMain extends BasicGame{
	
	private List <PatternActor> actoors;
	private List <ObservPatternn> observv;
	
	public PatternMain(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		// TODO Auto-generated method stub
		for (PatternActor aactor : actoors) { aactor.draw(g);}
	}
	
	@Override
	public void init(GameContainer arg0) throws SlickException {
		// TODO Auto-generated method stub
		
		StrategyyPattern strategy2 = new Moveleft();
		StrategyyPattern strategy1 = new Moveright();
		actoors = new ArrayList<>();
		observv = new ArrayList<>();
		
		FactoryyPattern fa = new FactoryyPattern();
		
		AbstractPatternActor o1 = fa.getShape("OVAL", strategy1);
		actoors.add(o1);
		observv.add(o1);
		
		AbstractPatternActor r1 = fa.getShape("RECTANGLE", strategy2);
		actoors.add(r1);
		observv.add(r1);
	}
	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// TODO Auto-generated method stub
		for (PatternActor actor : actoors) { actor.move(gc, delta);}
	}
	
	public static void main(String[] argv) {
		try { AppGameContainer container = new AppGameContainer(new PatternMain("Game"));
		container.setDisplayMode(800,600,false);
		container.start();
		} 
		catch (SlickException e) { e.printStackTrace();}
	}
	
	@Override
	public void keyPressed(int key, char c) {
		// TODO Auto-generated method stub
		if(key == Input.KEY_ENTER) 
		{
			for (ObservPatternn observer : this.observv) { observer.inform();
			SingletonnPattern.addPoints(10);}
		};
	}	
}
