package li.wohl.patterns;

import java.util.Random;

import org.newdawn.slick.GameContainer;

public class Moveleft implements StrategyyPattern {
	private float a,b;
	
	public Moveleft() {
		Random random = new Random();
		this.a = random.nextInt(400);
		this.b = random.nextInt(400);
		
	}

	@Override
	public void moving(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		if(this.a < 0)
		{
			Random random = new Random();
			this.a = gc.getWidth();
			this.b = random.nextInt(gc.getHeight());
		}
		else
		{
			this.a -= 0.2f * delta;
		}
	}

	@Override
	public float getX() {
		// TODO Auto-generated method stub
		return this.a;
	}

	@Override
	public float getY() {
		// TODO Auto-generated method stub
		return this.b;
	}
	
}
