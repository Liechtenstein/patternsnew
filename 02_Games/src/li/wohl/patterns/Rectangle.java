package li.wohl.patterns;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Rectangle extends AbstractPatternActor implements ObservPatternn{
	
	private int a,b;
	private StrategyyPattern strategy;
	 
	public Rectangle(StrategyyPattern strategy) {
		super(strategy);
		
		this.strategy = strategy;
		
		this.a = 100;
		this.b = 100;
	}

	@Override
	public void draw(Graphics g) {
		g.drawRect(strategy.getX(), strategy.getY(), this.a, this.b);
	}

	@Override
	public void inform() {
		System.out.println("I AM 1 RECTANGLE");
	}

	@Override
	public void move(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		
	}

}
