package li.wohl.patterns;

import org.newdawn.slick.GameContainer;

public interface PatternActor {
	public void draw(org.newdawn.slick.Graphics g);
	public void move(GameContainer gc, int delta);
}
