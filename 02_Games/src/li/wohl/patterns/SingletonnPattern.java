package li.wohl.patterns;

public class SingletonnPattern {

	private static SingletonnPattern Instance;
	private static int Points;
	
	private SingletonnPattern() {
		SingletonnPattern.Points = 0;
	}
	
	public static SingletonnPattern getInstance() {
		return SingletonnPattern.Instance;
	}
	
	public static void addPoints(int amount) {
		Points += amount;
	}

	public static int getPoints() {
		return Points;
	}
}
