package li.wohl.wintergame;

import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class MyMainGame extends BasicGame {

	private ArrayList<CirclesActor> actors;
	private SnowMan snowman;
	
	public MyMainGame() {
		super("game");
	}
	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
	
			for(int i=0; i<this.actors.size();i++){
				actors.get(i).render(g);
			}
			this.snowman.render(g);
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
	
		
		this.actors = new ArrayList<>();
			
			for(int i=0;i<40;i++){
				Random random=new Random();
				
				int width=random.nextInt(800);
				int height=random.nextInt(600);
				
				this.actors.add(new CirclesActor(width,height,10,0.07f));
			}
			
			
			for(int i=0;i<50;i++){
				Random random=new Random();
				
				int width=random.nextInt(800);
				int height=random.nextInt(600);
				
				this.actors.add(new CirclesActor(width,height,8,0.1f));
			}
			
			for(int i=0;i<40;i++){
				Random random=new Random();
				
				int width=random.nextInt(800);
				int height=random.nextInt(600);
				
				this.actors.add(new CirclesActor(width,height,4,0.09f));
			}
			
			for(int i=0;i<40;i++){
				Random random=new Random();
				
				int width=random.nextInt(800);
				int height=random.nextInt(600);
				
				this.actors.add(new CirclesActor(width,height,2,0.05f));
			}
			
			this.snowman = new SnowMan(400,400);
	}

	@Override
	public void update(GameContainer gc, int millisSinceLastCall) throws SlickException {
		for(int i=0; i<this.actors.size();i++){
			actors.get(i).update(millisSinceLastCall);
		}

	}
	
	@Override
	public void keyPressed(int key, char c) {
		super.keyPressed(key, c);
		System.out.println(key);
		
		if(key == 203 ||c== 'a'){
			this.snowman.moveLeft();
		}
		
		if(key == 205 ||c== 'd'){
			this.snowman.moveRight();
		}
		
		
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MyMainGame());
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
