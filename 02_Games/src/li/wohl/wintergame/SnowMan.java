package li.wohl.wintergame;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class SnowMan {

	private float x,y;
	private Image image;
	
	public SnowMan(float x, float y) throws SlickException{
		super();
		this.x = x;
		this.y = y;
		
		this.image = new Image("testdata/grass.png");
	}
	
	public void render(Graphics g){
		this.image.draw(this.x, this.y, 90, 120);
	}
	
	public void moveRight(){
		this.x = this.x + 10;
	}
	
	public void moveLeft(){
		this.x = this.x - 10;
	}
}
