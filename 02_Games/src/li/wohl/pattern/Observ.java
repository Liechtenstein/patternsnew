package li.wohl.pattern;

public interface Observ {

	public void inform();
	
}
