package li.wohl.pattern;

import java.awt.List;
import java.util.ArrayList;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.util.pathfinding.navmesh.Space;

import li.wohl.pattern.MoveLeft;
import li.wohl.pattern.MoveRight;
import li.wohl.pattern.Strategyy;

public class MainPatternGame extends BasicGame {
	
	private ArrayList <Actr> actors;
	private ArrayList <Observ> observers;
	
	public MainPatternGame(String Main) {
		super(Main);
	}
	
	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		for (Actr actorr : actors) {
			actorr.draw(g);
		}
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		Strategyy strategyymove1 = new MoveLeft();
		Strategyy strategyymove2 = new MoveRight();
		
		Factoryy f = new Factoryy();
		
		observers = new ArrayList<>();
		actors = new ArrayList<>();
		
		AbstractActr Circle1 = f.getShape("Oval_Circle", strategyymove1);
		AbstractActr Rectangle1 = f.getShape("Rectangle", strategyymove2);
		
		actors.add(Circle1);
		actors.add(Rectangle1);
		
		actors.add(Circle1);
		observers.add(Rectangle1);
	}
	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		for (Actr actorr : actors) {
			actorr.moving(gc, delta);
		}
	}
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainPatternGame("Game"));
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void keyPressed(int key, char c) {
		if(key == Input.KEY_SPACE) {
			for (Observ observer : this.observers) {
				observer.inform();
				Singletonn.addPoints(10);
			}
		}
	}
}
