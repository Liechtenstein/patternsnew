package li.wohl.pattern;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public interface Actr {
	public void draw(Graphics g);
	public void moving(GameContainer gc, int delta);
}
