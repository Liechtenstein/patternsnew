package li.wohl.pattern;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class CircleA extends AbstractActr{
	private int a, b;
	private Strategyy strategyymove;

	public CircleA(Strategyy strategyymove){ 
		super(strategyymove);

		this.strategyymove = strategyymove;
		this.a = 100;
		this.b = 100;
	}
	
	public void draw(Graphics g) {
		g.setColor(Color.white);
		g.drawOval(strategyymove.getX(), strategyymove.getY(), this.a, this.b);
	}
	@Override
	public void inform() {
		System.out.println("HELLO! IAMANACTOR + CIRCLE");
	}
}
