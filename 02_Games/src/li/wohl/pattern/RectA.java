package li.wohl.pattern;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import java.util.Random;

public class RectA extends AbstractActr implements Observ{
	
	private int a,b;
	private Strategyy strategyymove;

	
	public RectA(Strategyy strategyymove){
		super(strategyymove);
		
		this.strategyymove = strategyymove;
		
		this.a = 100;
		this.b = 100;
	}

	public void draw(Graphics g) {
		g.setColor(Color.white);
		g.drawRect(this.strategyymove.getX(), this.strategyymove.getY(), this.a, this.b);
	}

	@Override
	public void inform() {
		System.out.println("HELLO! IAMANACTOR + RECTANGLE");
	}
}
