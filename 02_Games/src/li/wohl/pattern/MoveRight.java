package li.wohl.pattern;

import java.util.Random;
import org.newdawn.slick.GameContainer;

public class MoveRight implements Strategyy{
	private float x,y;
	public MoveRight() {
		Random random = new Random();
		this.x = random.nextInt(400);
		this.y = random.nextInt(400);
	}
	@Override
	public void moving(GameContainer gc, int delta) {
		if(this.x > gc.getWidth()){
			Random random = new Random();
			this.x = 0;
			this.y = random.nextInt(gc.getHeight());
		}
		else{
			this.x += 0.5f * delta;
		}
	}
	@Override
	public float getX() {
				return this.x;
	}
	@Override
	public float getY() {
				return this.y;
	}
}
