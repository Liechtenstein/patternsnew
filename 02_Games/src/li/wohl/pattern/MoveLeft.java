package li.wohl.pattern;

import java.util.Random;
import org.newdawn.slick.GameContainer;


public class MoveLeft implements Strategyy{
	private float x,y;
	public MoveLeft() {
		Random random = new Random();
		this.x = random.nextInt(400);
		this.y = random.nextInt(400);
	}
	@Override
	public void moving(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		if(this.x < 0){
			Random random = new Random();
			this.x = gc.getWidth();
			this.y = random.nextInt(gc.getHeight());
		}
		else{
			this.x -= 0.2f * delta;
		}
	}
	@Override
	public float getX() {
		return this.x;
	}
	@Override
	public float getY() {
		return this.y;
	}
}
