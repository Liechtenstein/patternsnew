package li.wohl.pattern;

import org.newdawn.slick.GameContainer;

public abstract class AbstractActr implements Actr, Observ {
	
	private Strategyy strategyymove;

	public AbstractActr(Strategyy strategyymove) {
		this.strategyymove = strategyymove;
	}
	
	public void moving(GameContainer gc, int delta) {
		strategyymove.moving(gc, delta);
	}
}
