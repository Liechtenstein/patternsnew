package li.wohl.pattern;

import org.newdawn.slick.GameContainer;

public interface Strategyy {
	public void moving(GameContainer gc, int delta);
	
	public float getX();
	public float getY();
}
