package li.wohl.pattern;

import li.wohl.pattern.Strategyy;

public class Factoryy {
	
	public AbstractActr getShape(String ActorType, Strategyy strategyymove) {
		if (ActorType == null) {
			return null;
		}
		if (ActorType.equalsIgnoreCase("Kreis")){
			return new CircleA(strategyymove);

		} else if (ActorType.equalsIgnoreCase("Rechteck")){
			return new RectA(strategyymove);
		}
		return null;
	}
}
