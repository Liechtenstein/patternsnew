package li.wohl.pattern;

public class Singletonn {

	private static Singletonn Instance;
	private static int Points;
	
	private Singletonn() {
		Singletonn.Points = 0;
	}
	
	public static Singletonn getInstance() {
		return Singletonn.Instance;
	}
	
	public static void addPoints(int amount) {
		Points += amount;
	}

	public static int getPoints() {
		return Points;
	}
	
}
